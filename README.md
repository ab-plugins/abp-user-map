# ABP User Map
[![ko-fi](https://www.ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/crazycat)

Creates a new page on MyBB showing the user map
![example page](Documents/page1.png)
# Features
## Usermap page
A simple page showing the map and clusters/markers. Clicking a marker will show the username and his avatar.

## ACP
* choose the default location (the center of the map when initialised)
* choose the default zoom level
* choose the marker

## User CP
The subpage "your location" will allow the user to place/move/delete a marker on his location.

# Installation - Upgrade
* Unzip the archive
* Upload the content of the Upload directory to your forum root
* **New installation** : install the plugin from your ACP
* **Upgrade** : deactivate and reactivate the plugin from your ACP - Go to *Configuration* >> *User Map* and click on the *Upgrade DB* tab

**Don not forget**: There is two configurations, one in the normal plugin setting way and the other (dedicated to the map itself) in a separate admin page.

# Styling
You can now add a custom class to styling the popup. The class can be nammed as you want put must have 2 childs: **.leaflet-popup-tip** and **.leaflet-popup-content-wrapper** to be sure the colors you modify will give an homogen result.
## class example
```css
.custom .leaflet-popup-tip, .custom .leaflet-popup-content-wrapper {
   background: #efefef;
   font-style: italic;
}
```

# Ressources
* [Open Street Map](https://www.openstreetmap.org/)
* [Leaflet](https://leafletjs.com/)
* [ip-api](https://ip-api.com)
