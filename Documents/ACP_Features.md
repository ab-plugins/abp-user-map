# Global settings
*(Home > Board Settings > Umap settings)*
* [x] Groups allowed to see the usermap (groupselect) def. all but unregistered, waiting and banned
* [x] Display group color (yesno) def. no
* [x] Display user avatar (yesno) def. yes
* [x] link to userpage (yesno) def. yes
* [x] warn user if they didn't localise themself (yesno) def. no

# Usermap fine settings
*(Home > ABP Usermap)*
* [x] Default map center (GUI)
* [x] Default coordinates => text field, **automatically set when moving map**
* [x] Default zoom => numeric field, **automatically changed when (de)zooming map**
* [x] Default marker => **text field**, or select in directory list ?
* [ ] Group marker => Select different marker, depending on group ? If not set, uses default ?
