<?php
/**
 * Task to force localisation of users
 * Copyright 2020 CrazyCat <crazycat@c-p-f.org>
 */

/************ INSTALL 
 Just create a task using abp_umap.php
 Set Time: Minutes to a real value (not *)
 ************/
 
if (!defined("IN_MYBB")) {
    die("Direct initialization of this file is not allowed.<br /><br />Please make sure IN_MYBB is defined.");
}
require_once MYBB_ROOT . '/inc/plugins/'.basename(__FILE__);

function task_abp_umap($task) {
    global $mybb, $lang, $db;
	$cptl = $cptu = 0;
	if (!$db->table_exists(CN_ABPUMAP.'users')) {
		add_task_log($task, 'ABP Umap is not installed');
		return;
	}
	// First part : integrate users which weren't localized
	$query = $db->write_query("SELECT u.uid, u.regip, u.lastip FROM ".TABLE_PREFIX."users u LEFT JOIN ".TABLE_PREFIX.CN_ABPUMAP."users g ON (g.uid=u.uid) WHERE g.hide IS NULL AND u.regip<>'' AND u.lastip<>'' LIMIT 10");
	while ($user = $db->fetch_array($query)) {
		if (my_inet_ntop($db->unescape_binary($user['lastip']))=='') {
			$user['ip'] = my_inet_ntop($db->unescape_binary($user['regip']));
		} else {
			$user['ip'] = my_inet_ntop($db->unescape_binary($user['lastip']));
		}
		debug($user);
		$latlon = abp_umap_autolocate($user['uid'], 1, $user['ip']);
		abp_umap_fillufield((int) $user['uid']);
		$cptl++;
	}
	// Second part : find city for old users
	$query2 = $db->simple_select(CN_ABPUMAP.'users', 'uid, lat, lon', "zip IS NULL AND lat<>0.00000 AND lon<>0.00000", ['limit' => 10]);
	while ($user = $db->fetch_array($query2)) {
		debug($user);
		abp_umap_geo2loc((int) $user['uid'], $user['lat'], $user['lon'], true);
		abp_umap_fillufield((int) $user['uid']);
		$cptu++;
	}
	add_task_log($task, 'ABP Umap ended successfully with '.$cptl.' users located and '.$cptu.' cities updated');
}
