<?php
/**
 * MyBB Copyright 2014 MyBB Group, All Rights Reserved
 *
 * Website: https://mybb.com
 * License: https://mybb.com/about/license
 *
 */

// --------------------------------------------------------------------------------
// MyBB Language Pack File.
// This file has been generated by MyBB - buildin language pack editor.
// ================================================================================
// Friendly name of the language : French (Français)
// Author of the language pack : MyBB.fr
// Language pack translators website : https://www.mybb.fr/
// Compatible version of MyBB : 1818
// Last edited in MyBB Editor by : CrazyCat
// Last edited date : Thu, 11 Jun 2020 14:34:57 +0000
// --------------------------------------------------------------------------------

$l['abp_umapname'] = 'Carte utilisateurs';
$l['abp_umap_btn_update'] = 'Valider';
$l['abp_umap_btn_delete'] = 'Supprimer';
$l['abp_umap_updated'] = 'Votre emplacement est sauvegardé';
$l['abp_umap_deleted'] = 'Votre emplacement est supprimé';
$l['abp_umap_reseted'] = 'Votre emplacement est réinitialisé';
$l['abp_umap_what'] = 'Je ne comprends pas votre demande';
$l['abp_umap_ucpdesc'] = 'Déplacez l\'épingle sur votre emplacement et validez. Vous apparaitrez sur la <a href="./misc.php?action=abp_umap">carte des utilisateurs</a>.';
$l['abp_umap_confirm'] = 'Confirmation :';
$l['abp_umap_updateme'] = 'Cochez-moi pour permettre la validation de la modification';
$l['abp_umap_deleteme'] = 'Cochez-moi pour permettre la suppression de votre emplacement';
$l['abp_umap_hideme'] = 'Cochez-moi pour cacher votre emplacement aux autres utilisateurs';
$l['abp_umap_cannotdel'] = 'Vous ne pouvez pas modifier ET supprimer';
$l['abp_umap_title_update'] = 'Mettre à jour l\'emplacement';
$l['abp_umap_title_delete'] = 'Supprimer l\'emplacement';
$l['abp_umap_title_hide'] = 'Cacher mon emplacement';
$l['abp_umap_action'] = 'Action :';
$l['abp_umap_changeloc'] = 'Mettre à jour';
$l['abp_umap_deleteloc'] = 'Supprimer';
$l['abp_umap_resetloc'] = 'Géolocaliser';
$l['abp_umap_donothing'] = 'Ne rien faire';
$l['abp_umap_wol'] = 'Regarde la <a href="{1}">carte utilisateurs</a>';
$l['osm_credit'] = '&copy; <a href="https://www.openstreetmap.org" title="OpenStreetMap">OpenStreetMap</a> contributors';
$l['abp_umap_page_title'] = 'Carte des utilisateurs';
$l['abp_umap_stats'] = 'Actuellement, {1} utilisateurs ont indiqué leur position.';
$l['abp_umap_userlist'] = '<li>Les utilisateurs positionnés sont : {1}</li>';
$l['abp_umap_centerme'] = 'Ma position';
$l['abp_umap_ucp_notice_title'] = 'Ma position sur la carte';
$l['abp_umap_ucp_notice'] = 'Ici, vous pouvez très facilement indiquer votre position.<br />Déplacez simplement le repère, cochez la case de confirmation et validez. Vous pouvez zoomer la carte pour plus de précision.<br />Vous pouvez aussi supprimer votre localisation si vous ne souhaitez plus qu\'elle soit visible pour les autres utilisateurs.';
$l['abp_umap_location_not_set'] = '<div class="red_alert">Vous n\'avez pas indiqué votre position</div>';
$l['abp_umap_rzoom'] = 'Revenir au niveau de zoom initial';
$l['abp_umap_gps'] = 'Coordonnées GPS (information)';
$l['abp_umap_recenter'] = 'Recentrer la carte';
$l['abp_umap_warningnotset'] = 'Vous n\'avez pas indiqué votre position. Vous pouvez le faire depuis <a href="{1}/usercp.php?action=abp_umap">votre panneau de contrôle</a>.';
