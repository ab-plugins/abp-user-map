<?php

/**
 * Usermap
 * (c) CrazyCat 2019 - 2021
 */
if (!defined('IN_MYBB')) {
    die('Direct initialization of this file is not allowed.<br /><br />Please make sure IN_MYBB is defined.');
}

define('CN_ABPUMAP', str_replace('.php', '', basename(__FILE__)));
// UMAP_TPL will be used only for templates
// to avoid _ and allows to use group
define('UMAP_TPL', str_replace('_', '', CN_ABPUMAP));

if (!defined('THIS_SCRIPT')) {
    define('THIS_SCRIPT', 'dummy.php');
}

/**
 * Initialization of the info
 * @global object $lang
 * @return array
 */
function abp_umap_info() {
    global $lang;
    $lang->load(CN_ABPUMAP);
    return array(
        'name' => $lang->abp_umapname,
        'description' => $lang->abp_umapdesc . '<a href=\'https://ko-fi.com/V7V7E5W8\' target=\'_blank\'><img height=\'30\' style=\'border:0px;height:30px;float:right;\' src=\'https://az743702.vo.msecnd.net/cdn/kofi1.png?v=0\' border=\'0\' alt=\'Buy Me a Coffee at ko-fi.com\' /></a>',
        'website' => 'https://gitlab.com/ab-plugins/abp-user-map',
        'author' => 'CrazyCat',
        'authorsite' => 'http://community.mybb.com/mods.php?action=profile&uid=6880',
        'version' => '2.1.1',
        'compatibility' => '18*',
        'codename' => CN_ABPUMAP
    );
}

/**
 * Basics : (de)install, (de)activate
 */

/**
 * Install procedure
 * Adds settings and userdb field
 * @global DB_MySQLi $db
 * @global MyLanguage $lang
 * @global datacache $cache
 */
function abp_umap_install() {
    global $db, $lang;
    $lang->load(CN_ABPUMAP);
    $settinggroups = ['name' => CN_ABPUMAP,
        'title' => $lang->abp_umap_setting_title,
        'description' => $lang->abp_umap_setting_desc,
        'disporder' => 0,
        "isdefault" => 0
    ];
    $db->insert_query('settinggroups', $settinggroups);
    $gid = $db->insert_id();
    // New global settings
    abp_umap_upgrade($gid);
    rebuild_settings();
    $db->write_query("CREATE TABLE IF NOT EXISTS " . TABLE_PREFIX . CN_ABPUMAP . " ( sname VARCHAR(50) NOT NULL , svalue VARCHAR(200) NOT NULL , UNIQUE sname (sname))");
    $db->insert_query_multiple(CN_ABPUMAP, [
        ['sname' => 'latlng', 'svalue' => '43.3,1.9'],
        ['sname' => 'zoom', 'svalue' => 6],
        ['sname' => 'marker', 'svalue' => '/images/marker.png']
    ]);
}

/**
 * 
 * @global MyLanguage $lang
 * @global DB_MySQLi $db
 * @param int $gid
 */
function abp_umap_upgrade($gid = 0) {
    global $lang, $db;
    $lang->load(CN_ABPUMAP);
    $settings = [
        [
            'name' => CN_ABPUMAP . '_gallowed',
            'title' => $lang->abp_umap_gallowed,
            'description' => $lang->abp_umap_gallowed_desc,
            'optionscode' => 'groupselect',
            'value' => '2,3,4,6',
            'disporder' => 1
        ],
        [
            'name' => CN_ABPUMAP . '_adisplay',
            'title' => $lang->abp_umap_adisplay,
            'description' => $lang->abp_umap_adisplay_desc,
            'optionscode' => 'yesno',
            'value' => 1,
            'disporder' => 2
        ],
        [
            'name' => CN_ABPUMAP . '_ulink',
            'title' => $lang->abp_umap_ulink,
            'description' => $lang->abp_umap_ulink_desc,
            'optionscode' => 'yesno',
            'value' => 1,
            'disporder' => 3
        ],
        [
            'name' => CN_ABPUMAP . '_gcolor',
            'title' => $lang->abp_umap_gcolor,
            'description' => $lang->abp_umap_gcolor_desc,
            'optionscode' => 'yesno',
            'value' => 0,
            'disporder' => 4
        ],
        [
            'name' => CN_ABPUMAP . '_userlist',
            'title' => $lang->abp_umap_userlist,
            'description' => $lang->abp_umap_userlist_desc,
            'optionscode' => 'yesno',
            'value' => 0,
            'disporder' => 5
        ],
        [
            'name' => CN_ABPUMAP . '_usersort',
            'title' => $lang->abp_umap_usersort,
            'description' => $lang->abp_umap_usersort_desc,
            'optionscode' => 'select
regdate=' . $lang->abp_umap_joined . '
username=' . $lang->abp_umap_username . '
lastvisit=' . $lang->abp_umap_lastvisit,
            'value' => 'regdate',
            'disporder' => 6
        ],
        [
            'name' => CN_ABPUMAP . '_usorder',
            'title' => $lang->abp_umap_sortorder,
            'description' => $lang->abp_umap_sortorderdesc,
            'optionscode' => 'radio
asc=' . $lang->abp_umap_asc . '
desc=' . $lang->abp_umap_desc,
            'value' => 'asc',
            'disporder' => 7
        ],
        [
            'name' => CN_ABPUMAP . '_warnuser',
            'title' => $lang->abp_umap_warnuser,
            'description' => $lang->abp_umap_warnuserdesc,
            'optionscode' => 'yesno',
            'value' => 0,
            'disporder' => 8
        ],
        [
            'name' => CN_ABPUMAP . '_autolocate',
            'title' => $lang->abp_umap_autolocate,
            'description' => $lang->abp_umap_autolocate_desc,
            'optionscode' => 'yesno',
            'value' => 0,
            'disporder' => 9
        ],
        [
            'name' => CN_ABPUMAP . '_gallowhide',
            'title' => $lang->abp_umap_gallowhide,
            'description' => $lang->abp_umap_gallowhide_desc,
            'optionscode' => 'groupselect',
            'value' => '2,3,4,6',
            'disporder' => 10
        ],
        [
            'name' => CN_ABPUMAP . '_blockhidders',
            'title' => $lang->abp_umap_blockhidders,
            'description' => $lang->abp_umap_blockhidders_desc,
            'optionscode' => 'yesno',
            'value' => 0,
            'disporder' => 11
        ],
        [
            'name' => CN_ABPUMAP . '_hiddedlist',
            'title' => $lang->abp_umap_hiddedlist,
            'description' => $lang->abp_umap_hiddedlist_desc,
            'optionscode' => 'groupselect',
            'value' => '3,4',
            'disporder' => 12
        ]
    ];
    $osettings = [];
    if ((int) $gid == 0) {
        $query = $db->simple_select('settings', 'name, gid', "name LIKE '" . CN_ABPUMAP . "%'");
        while ($setted = $db->fetch_array($query)) {
            $osettings[] = $setted['name'];
            $gid = $setted['gid'];
        }
    }
    foreach ($settings as $i => $setting) {
        if (in_array($setting['name'], $osettings)) {
            continue;
        }
        $insert = [
            'name' => $db->escape_string($setting['name']),
            'title' => $db->escape_string($setting['title']),
            'description' => $db->escape_string($setting['description']),
            'optionscode' => $db->escape_string($setting['optionscode']),
            'value' => $db->escape_string($setting['value']),
            'disporder' => $setting['disporder'],
            'gid' => $gid,
        ];
        $db->insert_query('settings', $insert);
    }

    if (!$db->table_exists(CN_ABPUMAP . 'users')) {
        $db->write_query("CREATE TABLE " . TABLE_PREFIX . CN_ABPUMAP . "users (uid INT(11) NOT NULL, lat FLOAT(8,5) NOT NULL DEFAULT 0.00000, lon FLOAT(8,5) NOT NULL DEFAULT 0.00000, hide TINYINT NOT NULL DEFAULT 0, country VARCHAR(250) NULL, countrycode VARCHAR(4) NULL, region VARCHAR(200) NULL, city VARCHAR(250) NULL, zip VARCHAR(10) NULL, autoloc TINYINT NOT NULL DEFAULT 0)");
        $db->write_query("ALTER TABLE " . TABLE_PREFIX . CN_ABPUMAP . "users ADD PRIMARY KEY (uid), ADD KEY latlon (lat,lon)");
    }
    if ($db->field_exists('aum_latlng', 'users')) {
        // Version 1, we must upgrade
        if ($db->field_exists('aum_hide', 'users')) {
            $query = "INSERT INTO " . TABLE_PREFIX . CN_ABPUMAP . "users (uid, lat, lon, hide) SELECT uid, SUBSTRING_INDEX(aum_latlng, ',', 1) AS lat, SUBSTRING_INDEX(aum_latlng, ',', -1) AS lon, aum_hide FROM mybb_users WHERE aum_latlng IS NOT NULL";
        } else {
            $query = "INSERT INTO " . TABLE_PREFIX . CN_ABPUMAP . "users (uid, lat, lon) SELECT uid, SUBSTRING_INDEX(aum_latlng, ',', 1) AS lat, SUBSTRING_INDEX(aum_latlng, ',', -1) AS lon FROM mybb_users WHERE aum_latlng IS NOT NULL";
        }
        $db->write_query($query);
        $db->write_query("ALTER TABLE " . TABLE_PREFIX . "users DROP COLUMN aum_latlng");
        if ($db->field_exists('aum_hide', 'users')) {
            $db->write_query("ALTER TABLE " . TABLE_PREFIX . "users DROP COLUMN aum_hide");
        }
    }
}

/**
 * Checks if plugin is installed
 * @global DB_MySQLi $db
 * @return boolean
 */
function abp_umap_is_installed() {
    global $db;
    return ($db->table_exists(CN_ABPUMAP) || $db->field_exists('aum_latlng', 'users'));
}

/**
 * Uninstall function
 * @global DB_MySQLi $db
 */
function abp_umap_uninstall() {
    global $db, $cache;
    $db->delete_query('settings', "name LIKE '" . CN_ABPUMAP . "_%'");
    $db->delete_query('settinggroups', "name = '" . CN_ABPUMAP . "'");
    rebuild_settings();
    abp_umap_deactivate();
    $db->write_query("DROP TABLE IF EXISTS " . TABLE_PREFIX . CN_ABPUMAP . "users");
    $db->write_query("DROP TABLE IF EXISTS " . TABLE_PREFIX . CN_ABPUMAP);
    $db->drop_table(CN_ABPUMAP);
    $cache->delete(CN_ABPUMAP);
}

/**
 * Activation function : creates the templates
 * @global DB_MySQLi $db
 * @global MyLanguage $lang
 */
function abp_umap_activate() {
    global $db, $lang;
    $db->insert_query('templategroups', ['prefix' => UMAP_TPL, 'title' => $lang->abp_umapname, 'isdefault' => 1]);
    $templates = [
        [
            'title' => UMAP_TPL . '_page',
            'template' => '<head>
<title>{$mybb->settings[\\\'bbname\\\']} - {\$lang->abp_umapname}</title>
{$headerinclude}
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/leaflet/1.7.1/leaflet.css" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/leaflet.markercluster/1.4.1/MarkerCluster.css" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/leaflet.markercluster/1.4.1/MarkerCluster.Default.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/leaflet/1.7.1/leaflet.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/leaflet.markercluster/1.4.1/leaflet.markercluster.js"></script>
</head>
<body>
{$header}
<table border="0" cellspacing="{\$theme[\\\'borderwidth\\\']}" cellpadding="{\$theme[\\\'tablespace\\\']}" class="tborder">
    <tbody>
        <tr><td class="thead"><strong>{\$lang->abp_umap_page_title}</strog></td></tr>
        <tr><td class="trow1"><div id="abp_usermap" style="height:600px;"></div></td></tr>
        <tr><td class="trow1">
            <input class="button" type="button" name="rzoom" id="rzoom" value="{\$lang->abp_umap_rzoom}" />
            <input class="button" type="button" name="centerme" id="centerme" value="{\$abp_umap_recenter}" />
        </td></tr>
        <tr><td class="trow1"><ul>
            <li>{\$abp_umap_stats}</li>
            {\$abp_umap_userlist}
        </ul></td></tr>
    </tbody>
</table>
<script type="text/javascript">
    var center = {{$umap_center}};
    var abp_umap = null;
    var markCluster;
    var markers = [];
    {\$umap_users}
    function init_map() {
            abp_umap = L.map("abp_usermap").setView([center.lat, center.lon], {\$umap_zoom});
            markCluster = L.markerClusterGroup({\$disabcluster});
            L.tileLayer("//{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {attribution: \\\'{\$lang->osm_credit}\\\'}).addTo(abp_umap);
            {\$scalecontrol}
            for (user in umusers) {
                var uIcon = L.icon({ iconUrl:"{\$mybb->settings[\\\'bburl\\\']}{$umap_marker}", iconSize:[{\$iw}, {\$ih}], iconAnchor: [{\$hof},{\$vof}], popupAnchor :[({\$iw}/2)-{\$hof},-{\$vof}]});
                var marker = L.marker([umusers[user].lat, umusers[user].lon], { icon: uIcon }).bindPopup(umusers[user].img, {className:\\\'{\$umap_class}\\\'});
                markers[parseInt(user)] = marker;
                markCluster.addLayer(marker);
            }
            abp_umap.addLayer(markCluster);
    }
    jQuery(function(\$) {
            // Adding a comment here
            init_map();
            $("#centerme").click(function() { abp_umap.setView([{\$ulat}, {\$ulon}]); return false; });
            $("#rzoom").click(function() { abp_umap.setZoom(\$umap_zoom); return false; });
            $(".popmap").on("click", function(e) {
                if (1=={\$usettings[\\\'linktype\\\']}) {
                    e.preventDefault();
                    tmpuid = parseInt(this.id);
                    popuser = markers[tmpuid];
                    abp_umap.setView([umusers[tmpuid].lat, umusers[tmpuid].lon], {\$userzoom});
                    popuser.openPopup();
                    return false;
                }
            });
        });
</script>
{$footer}
</body>
</html>',
            'sid' => -2,
            'version' => 1.5,
            'dateline' => TIME_NOW
        ],
        [
            'title' => UMAP_TPL . '_ucp',
            'template' => '<head>
<title>{\$mybb->settings[\\\'bbname\\\']} - {\$lang->abp_umapname}</title>
{$headerinclude}
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/leaflet/1.7.1/leaflet.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/leaflet/1.7.1/leaflet.js"></script>
</head>
<body>
{$header}
<form name="umap" method="post" action="usercp.php?action=do_abp_umap" />
<input type=\"hidden\" name=\"my_post_key\" value=\"{\$mybb->post_code}\" />
<table width="100%" border="0" align="center">
    <tr>
    {\$usercpnav}
    <td align="center" valign="top">
        <table border="0" cellspacing="{\$theme[\\\'borderwidth\\\']}" cellpadding="{\$theme[\\\'tablespace\\\']}" class="tborder">
        <tr><td class="thead"><strong>{\$lang->abp_umapname}</strong></td></tr>
        <tr><td class="tcat"><strong>{\$lang->abp_umap_ucp_notice_title}</strong></td></tr>
        <tr><td class="trow1">{\$abp_umap_location_not_set}{\$lang->abp_umap_ucp_notice}</td></tr>
        <tr><td class="tcat"><strong>{\$lang->abp_umap_title_update}</strong></td></tr>
        <tr><td class="trow1">
        <div id="abp_usermap" style="height:600px;"></div>
        </td></tr>
        <tr><td class="trow1">
            <strong>{\$lang->abp_umap_gps}</strong> <input type="text" class="textbox" readonly="readonly" name="ulatlng" id="ulatlng" value="{\$ulatlng}" size="40" />
            <input class="button" type="button" name="rzoom" id="rzoom" value="{\$lang->abp_umap_rzoom}" />
            <input class="button" type="button" name="centerme" id="centerme" value="{\$lang->abp_umap_recenter}" />
        </td></tr>
        <tr><td class="trow1"><strong>{\$lang->abp_umap_action}</strong> <input type="radio" id="umapact[0]" name="umapact" value="change" />{\$lang->abp_umap_changeloc} <input type="radio" id="umapact[1]" name="umapact" value="delete" />{\$lang->abp_umap_deleteloc} <input type="radio" id="umapact[2]" name="umapact" value="nothing" checked="checked" />{\$lang->abp_umap_donothing}
            {\$umaphide}
        <tr><td class="trow1" style="text-align: center;"><input class="button" type="submit" value="{\$lang->abp_umap_btn_update}" /></td></tr>
        </table>
    </td>
    </tr>
</table>
</form>
<script type="text/javascript">
    var center = {{$umap_center}};
    var abp_umap = null;
    function init_map() {
        abp_umap = L.map("abp_usermap").setView([center.lat, center.lon], {$umap_zoom});
        L.tileLayer("//{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", { attribution: \\\'{\$lang->osm_credit}\\\'}).addTo(abp_umap);
        {\$scalecontrol}
        var uIcon = L.icon({ iconUrl:"{\$mybb->settings[\\\'bburl\\\']}{$umap_marker}", iconSize:[{\$iw}, {\$ih}], iconAnchor: [{\$hof},{\$vof}]});
        var userMarker = L.marker([center.lat, center.lon], {draggable:true, icon: uIcon}).addTo(abp_umap).on(\\\'dragend\\\', function() {
            var coord = userMarker.getLatLng();
            $(\\\'#ulatlng\\\').val(coord.lat+\\\',\\\'+coord.lng);
        });
        
    }
    $(function() {
        init_map();
        $("#centerme").click(function() { abp_umap.setView([center.lat, center.lon]); return false; });
        $("#rzoom").click(function() { abp_umap.setZoom(\$umap_zoom); return false; });
    });
</script>
{$footer}
</body>
</html>',
            'sid' => -2,
            'version' => 1.5,
            'dateline' => TIME_NOW
        ],
        [
            'title' => UMAP_TPL . '_nav_option',
            'template' => '<tr><td class="trow1 smalltext"><a href="{\$mybb->settings[\\\'bburl\\\']}/usercp.php?action=abp_umap" class="usercp_nav_item" style="background-image: url(\\\'{\$mybb->settings[\\\'bburl\\\']}/images/osm.png\\\'); background-repeat: no-repeat;">{\$lang->abp_umapname}</a></td></tr>',
            'sid' => -2,
            'version' => 1.3,
            'dateline' => TIME_NOW
        ],
        [
            'title' => UMAP_TPL . '_toplink',
            'template' => '<li><a style="background-image: url(\\\'{\$mybb->settings[\\\'bburl\\\']}/images/osm.png\\\'); background-repeat: no-repeat;" href="{$mybb->settings[\\\'bburl\\\']}/misc.php?action=abp_umap">{\$lang->abp_umapname}</a></li>',
            'sid' => -2,
            'version' => 1.3,
            'dateline' => TIME_NOW
        ],
        [
            'title' => UMAP_TPL . '_popup',
            'template' => '{\$' . UMAP_TPL . '_popup_avatar}{\$username}',
            'sid' => -2,
            'version' => 1.3,
            'dateline' => TIME_NOW
        ],
        [
            'title' => UMAP_TPL . '_popup_avatar',
            'template' => '<img src="{\$avatar}" /><br />',
            'sid' => -2,
            'version' => 1.3,
            'dateline' => TIME_NOW
        ],
        [
            'title' => UMAP_TPL . '_warnuser',
            'template' => '<div class="error"><p><em>{\$abp_umap_warningnotset}</em></p></div><br />',
            'sid' => -2,
            'version' => 1.4,
            'dateline' => TIME_NOW
        ],
        [
            'title' => UMAP_TPL . '_hide',
            'template' => '<tr><td class="tcat"><strong>{\$lang->abp_umap_title_hide}</strong></td></tr>
        <tr><td class="trow1"><label for="uhide">{\$lang->abp_umap_hideme}</label><input type="checkbox" id="uhide" name="uhide"{\$hidechecked} /></td></tr>',
            'sid' => -2,
            'version' => 1.5,
            'dateline' => TIME_NOW
        ]
    ];
    foreach ($templates as $template) {
        $db->insert_query("templates", $template);
    }
    if (!$db->field_exists('aum_hide', 'users')) {
        $db->write_query("ALTER TABLE " . TABLE_PREFIX . "users ADD aum_hide TINYINT NOT NULL DEFAULT 0");
    }
    require_once MYBB_ROOT . '/inc/adminfunctions_templates.php';
    find_replace_templatesets("usercp_nav_misc", "#" . preg_quote('{$lang->ucp_nav_forum_subscriptions}</a></td></tr>') . "#i", '{$lang->ucp_nav_forum_subscriptions}</a></td></tr>{' . CN_ABPUMAP . '_nav_option}');
    find_replace_templatesets("header", "#" . preg_quote('{$menu_memberlist}') . "#i", '{$menu_memberlist}{$' . CN_ABPUMAP . '_toplink}');
    find_replace_templatesets("header", "#" . preg_quote('<br />') . "$#i", '<br />{$' . CN_ABPUMAP . '_warnuser}');
}

/**
 * Deactivation function
 * @global DB_MySQLi $db
 * @global MyLanguage $lang
 */
function abp_umap_deactivate() {
    global $db, $lang;
    $lang->load(CN_ABPUMAP);
    $db->delete_query('templates', "title LIKE '" . UMAP_TPL . "_%' AND sid=-2");
    $db->delete_query('templategroups', "prefix='" . UMAP_TPL . "'");
    require_once MYBB_ROOT . '/inc/adminfunctions_templates.php';
    find_replace_templatesets("usercp_nav_misc", "#" . preg_quote('{' . CN_ABPUMAP . '_nav_option}') . "#", '', 0);
    find_replace_templatesets("header", "#" . preg_quote('{$' . CN_ABPUMAP . '_toplink}') . "#", '', 0);
    find_replace_templatesets("header", "#" . preg_quote('{$' . CN_ABPUMAP . '_warnuser}') . "#", '', 0);
}

$plugins->add_hook("admin_config_menu", "abp_umap_admin_config_menu");

/**
 * Add a sub-menu item in ACP
 * @global MyLanguage $lang
 * @param array $sub_menu
 * @return array
 */
function abp_umap_admin_config_menu($sub_menu) {
    global $lang;
    $lang->load(CN_ABPUMAP);
    $sub_menu[] = [
        'id' => 'abp_umap',
        'title' => $lang->abp_umapname,
        'link' => 'index.php?module=config-' . CN_ABPUMAP
    ];
    return $sub_menu;
}

$plugins->add_hook('admin_config_action_handler', CN_ABPUMAP . '_admin_config_action_handler');

/**
 * 
 * @param type $actions
 */
function abp_umap_admin_config_action_handler(&$actions) {
    $actions[CN_ABPUMAP] = ['active' => CN_ABPUMAP, 'file' => ''];
}

$plugins->add_hook("admin_page_output_header", "abp_umap_admin_header");

function abp_umap_admin_header(&$args) {
    if ($args['this']->active_module == 'config' && $args['this']->active_action == CN_ABPUMAP) {
        $args['this']->extra_header .= '<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/leaflet/1.7.1/leaflet.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/leaflet/1.7.1/leaflet.js"></script>' . PHP_EOL;
    }
}

$plugins->add_hook("admin_load", "abp_umap_admin_load");

/**
 * Generates the config page
 * @global object $mybb
 * @global DefaultPage $page
 * @global MyLanguage $lang
 * @global DB_MySQLi $db
 */
function abp_umap_admin_load() {
    global $page, $lang, $db, $mybb;
    if ($mybb->input['module'] != 'config-' . CN_ABPUMAP) {
        return;
    }
    $lang->load(CN_ABPUMAP);
    if ($mybb->input['action'] == 'upgrade') {
        abp_umap_upgrade();
        flash_message('All right', 'success');
        admin_redirect('index.php?module=config-' . CN_ABPUMAP);
    }
    if ($mybb->input['action'] == 'edit' && $mybb->request_method == 'post') {
        if (is_null($mybb->get_input('my_post_key')) || !verify_post_check($mybb->get_input('my_post_key'), true)) {
            flash_message('error verifying', 'error');
            admin_redirect('index.php?module=config-' . CN_ABPUMAP);
        }
        $nsettings = [];
        list($lat, $lng) = explode(',', $mybb->input['ulatlng']);
        $nsettings['latlng'] = $lat . ',' . $lng;
        $nsettings['zoom'] = filter_var($mybb->input['uzoom'], FILTER_SANITIZE_NUMBER_INT);
        $nsettings['marker'] = filter_var($mybb->input['marker'], FILTER_SANITIZE_STRING);
        // Check for marker : if it didn't exists, we must block the process
        if (!abp_marker_info($nsettings['marker'], true)) {
            flash_message($lang->abp_umap_marker_error, 'error');
            admin_redirect('index.php?module=config-' . CN_ABPUMAP);
        }
        $nsettings['hmarker'] = filter_var($mybb->input['hmarker'], FILTER_SANITIZE_STRING);
        $nsettings['vmarker'] = filter_var($mybb->input['vmarker'], FILTER_SANITIZE_STRING);
        $nsettings['scontrol'] = filter_var($mybb->input['scontrol'], FILTER_SANITIZE_STRING);
        $nsettings['disabcluster'] = filter_var($mybb->input['disabcluster'], FILTER_SANITIZE_NUMBER_INT);
        $nsettings['linktype'] = filter_var($mybb->input['linktype'], FILTER_SANITIZE_NUMBER_INT);
        $nsettings['uclass'] = filter_var($mybb->input['uclass'], FILTER_SANITIZE_STRING);
        $nsettings['defavatar'] = filter_var($mybb->input['defavatar'], FILTER_SANITIZE_NUMBER_INT);
        $nsettings['pfield'] = (int) filter_var($mybb->input['pfield'], FILTER_SANITIZE_NUMBER_INT);
        $nsettings['pfield_format'] = filter_var($mybb->input['pfield_format'], FILTER_SANITIZE_STRING);
        foreach ($nsettings as $sname => $svalue) {
            $db->replace_query(CN_ABPUMAP, ['svalue' => $svalue, 'sname' => $sname]);
        }
        flash_message('Data saved', 'success');
        admin_redirect('index.php?module=config-' . CN_ABPUMAP);
    }
    $settings = umap_get_settings();
    if (!isset($settings['hmarker'])) {
        $settings['hmarker'] = 'center';
    }
    if (!isset($settings['vmarker'])) {
        $settings['vmarker'] = 'middle';
    }
    list($lat, $lng) = explode(',', $settings['latlng']);

    $scalecontrol = '';
    if (isset($settings['scontrol']) && $settings['scontrol'] == 'yes') {
        $scalecontrol = 'L.control.scale().addTo(abp_umap);';
    } else {
        $settings['scontrol'] = 'no';
    }
    $disabcluster = 19;
    if (!isset($settings['disabcluster']) || (int) $settings['$disabcluster'] < 1 || (int) $settings['$disabcluster'] > 18) {
        $settings['$disabcluster'] = 19;
    }
    if (!isset($settings['linktype']) || $settings['linktype'] != 1) {
        $settings['linktype'] = 0;
    }
    if (!isset($settings['uclass'])) {
        $settings['uclass'] = '';
    }
    if (!isset($settings['defavatar']) || $settings['defavatar'] != 1) {
        $settings['defavatar'] = 0;
    }
    // @TODO : create function to get marker size
    list($iw, $ih) = abp_marker_info($settings['marker']);

    // Calculate offset depending on marker settings
    switch ($settings['hmarker']) {
        case 'left': $hof = $iw;
            break;
        case 'right': $hof = 0;
            break;
        default: $hof = $iw / 2;
            break;
    }
    switch ($settings['vmarker']) {
        case 'top': $vof = $ih;
            break;
        case 'bottom': $vof = 0;
            break;
        default: $vof = $ih / 2;
            break;
    }
    if (!isset($settings['pfield']) || (int) $settings['pfield'] < 1 || !array_key_exists((int) $settings['pfield'], getprofilefields())) {
        $settings['pfield'] = 0;
    }
    if (!isset($settings['pfield_format']) || trim($settings['pfield_format']) == '') {
        $settings['pfield_format'] = '%city% (%country%)';
    }

    $page->add_breadcrumb_item($lang->abp_umapname, 'index.php?module=config-' . CN_ABPUMAP);
    $page->output_header();

    $subtabs = [
        'abp_umap_options' => [
            'title' => $lang->abp_umap_taboption,
            'description' => $lang->abp_umap_taboption_desc,
            'link' => 'index.php?module=config-' . CN_ABPUMAP
        ],
        'abp_umap_tools' => [
            'title' => $lang->abp_umap_tabugrade,
            'description' => $lang->abp_umap_tabugrade_desc,
            'link' => 'index.php?module=config-' . CN_ABPUMAP . '&amp;action=upgrade'
        ]
    ];
    $page->output_nav_tabs($subtabs, 'abp_umap_options');

    $form = new Form('index.php?module=config-' . CN_ABPUMAP . '&action=edit', 'post');
    $table = new Table();
    $table->construct_header($lang->abp_umapname, ['colspan' => 2]);
    $table->construct_cell('<strong>' . $lang->abp_umap_latlng . '</strong><br />' . $lang->abp_umap_latlng_desc, ['colspan' => 2]);
    $table->construct_row();
    $table->construct_cell($form->generate_hidden_field('ulatlng', (float) $lat . ',' . (float) $lng, ['id' => 'ulatlng']) . PHP_EOL
            . '<div id="abp_usermap" style="height:600px;"></div>' . PHP_EOL
            . '<script type="text/javascript">
    var abp_umap = null;
    function init_map() {
        abp_umap = L.map("abp_usermap").setView([' . (float) $lat . ', ' . (float) $lng . '], ' . $settings['zoom'] . ');
        L.tileLayer("//{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", { attribution: \'&copy; <a href="https://www.openstreetmap.org" title="OpenStreetMap">OpenStreetMap</a> contributors\'}).addTo(abp_umap);
        ' . $scalecontrol . '
        var uIcon = L.icon({ iconUrl:"' . $mybb->settings['bburl'] . $settings['marker'] . '", iconSize:[' . $iw . ', ' . $ih . '], iconAnchor: [' . $hof . ',' . $vof . ']});
        var userMarker = L.marker([' . (float) $lat . ', ' . (float) $lng . '], {draggable:true, icon: uIcon}).addTo(abp_umap).on(\'dragend\', function() {
            var coord = userMarker.getLatLng();
            $(\'#ulatlng\').val(coord.lat+\',\'+coord.lng);
        });
    }
    $(function() { init_map();
        $(\'#uzoom\').change(function() { abp_umap.setZoom($(this).val()); });
        abp_umap.on(\'zoomend\', function() { $(\'#uzoom\').val(abp_umap.getZoom()); });
    });
</script>', ['colspan' => 2]);
    $table->construct_row();
    // Zoom selector
    $table->construct_cell('<strong>' . $lang->abp_umap_zoom . '</strong><br />' . $lang->abp_umap_zoom_desc);
    $table->construct_cell($form->generate_numeric_field('uzoom', $settings['zoom'], ['id' => 'uzoom', 'min' => 1, 'max' => 18]));
    $table->construct_row();
    // Marker icon
    $table->construct_cell('<strong>' . $lang->abp_umap_marker . '</strong><br />' . $lang->abp_umap_marker_desc);
    $table->construct_cell($form->generate_text_box('marker', $settings['marker']));
    $table->construct_row();
    // Marker H position
    $table->construct_cell('<strong>' . $lang->abp_umap_hmarker . '</strong><br />' . $lang->abp_umap_hmarker_desc);
    $table->construct_cell(
            $form->generate_select_box('hmarker', ['left' => $lang->left, 'center' => $lang->center, 'right' => $lang->right], $settings['hmarker'], ['id' => 'hmarker'])
    );
    $table->construct_row();
    // Marker V position
    $table->construct_cell('<strong>' . $lang->abp_umap_vmarker . '</strong><br />' . $lang->abp_umap_vmarker_desc);
    $table->construct_cell(
            $form->generate_select_box('vmarker', ['top' => $lang->top, 'middle' => $lang->middle, 'bottom' => $lang->bottom], $settings['vmarker'], ['id' => 'vmarker'])
    );
    $table->construct_row();
    // Scale control display
    $table->construct_cell('<strong>' . $lang->abp_umap_scontrol . '</strong><br />' . $lang->abp_umap_scontrol_desc);
    $table->construct_cell(
            $form->generate_yes_no_radio('scontrol', $settings['scontrol'], false)
    );
    $table->construct_row();
    // Disable clustering
    $table->construct_cell('<strong>' . $lang->abp_umap_disabcluster . '</strong><br />' . $lang->abp_umap_disabcluster_desc);
    $table->construct_cell($form->generate_numeric_field('disabcluster', $settings['disabcluster'], ['min' => 0, 'max' => 19]));
    $table->construct_row();
    // Link type for user list
    $table->construct_cell('<strong>' . $lang->abp_umap_linktype . ' (<em style="color:#c00; font-weight:bold;">beta</em>)</strong><br />' . $lang->abp_umap_linktype_desc);
    $table->construct_cell($form->generate_yes_no_radio('linktype', $settings['linktype'], true));
    $table->construct_row();
    // User class for popup
    $table->construct_cell('<strong>' . $lang->abp_umap_uclass . '</strong><br />' . $lang->abp_umap_uclass_desc);
    $table->construct_cell($form->generate_text_box('uclass', $settings['uclass']));
    $table->construct_row();
    // Force default avatar
    $table->construct_cell('<strong>' . $lang->abp_umap_defavatar . '</strong><br />' . $lang->abp_umap_defavatar_desc);
    $table->construct_cell(
            $form->generate_yes_no_radio('defavatar', $settings['defavatar'], true)
    );
    $table->construct_row();
    // Profile field
    $table->construct_cell('<strong>' . $lang->abp_umap_profilefield . '</strong><br />' . $lang->abp_umap_profilefield_desc);
    $table->construct_cell($form->generate_select_box('pfield', getprofilefields(), (int) $settings['pfield'], ['id' => 'pfield']));
    $table->construct_row();
    $table->construct_cell('<strong>' . $lang->abp_umap_profilemask . '</strong><br />' . $lang->abp_umap_profilemask_desc);
    $table->construct_cell($form->generate_text_box('pfield_format', $settings['pfield_format']));
    $table->construct_row();
    $table->output();
    $buttons = [];
    $buttons[] = $form->generate_submit_button($lang->abp_umap_btn_save);
    $form->output_submit_wrapper($buttons);
    $form->end();
    $page->output_footer();
}

function getprofilefields() {
    global $db;
    $pfields = [0 => '---'];
    $query = $db->simple_select('profilefields', 'fid, name', "1=1");
    while ($res = $db->fetch_array($query)) {
        $pfields[$res['fid']] = $res['name'];
    }
    return $pfields;
}

// Regenerate cache after changing settings in ACP
$plugins->add_hook('admin_config_settings_change_commit', CN_ABPUMAP . '_cache');

/**
 * @TODO : Add user info country / town => in usercp to allow a display of map
 * => use geonames ?
 */
/** User cp * */
$plugins->add_hook('usercp_menu_built', 'abp_umap_navoption', -10);

/**
 * Creates submenu
 * @global MyLanguage $lang
 */
function abp_umap_navoption() {
    global $mybb, $usercpnav, $lang, $templates, $usercpnav, $abp_umap_nav_option;
    $lang->load(CN_ABPUMAP);
    eval("\$abp_umap_nav_option = \"" . $templates->get(UMAP_TPL . '_nav_option') . "\";");
    $usercpnav = str_replace("{abp_umap_nav_option}", $abp_umap_nav_option, $usercpnav);
}

$plugins->add_hook('usercp_start', 'abp_umap_usercp');

/**
 * Creates UserCP page
 * @global MyLanguage $lang
 */
function abp_umap_usercp() {
    global $mybb, $db, $cache, $lang, $theme, $templates, $headerinclude, $header, $footer, $usercpnav;
    $lang->load(CN_ABPUMAP);
    if ($mybb->input['action'] == 'do_abp_umap') {
        verify_post_check($mybb->input['my_post_key']);
        $datas = ['hide' => 0, 'autoloc' => 0];
        if (isset($mybb->input['umapact']) && $mybb->input['umapact'] == 'change') {
            list($datas['lat'], $datas['lon']) = explode(',', $mybb->input['ulatlng']);
            abp_umap_geo2loc($mybb->user['uid'], $datas['lat'], $datas['lon']);
            abp_umap_fillufield($mybb->user['uid']);
            $message = $lang->abp_umap_updated;
        } elseif (isset($mybb->input['umapact']) && $mybb->input['umapact'] == 'delete') {
            $latlon = abp_umap_autolocate($mybb->user['uid'], 1);
            if ((int) $mybb->settings[CN_ABPUMAP . '_autolocate'] == 1) {
                list($datas['lat'], $datas['lon']) = explode(',', $latlon);
                $datas['autoloc'] = 1;
                abp_umap_fillufield($mybb->user['uid']);
                $message = $lang->abp_umap_reseted;
            } else {
                $datas = ['lat' => 0, 'lon' => 0, 'city' => '', 'region' => '', 'country' => '', 'countrycode' => '', 'zip' => null, 'autoloc' => 0];
                $message = $lang->abp_umap_deleted;
            }
        }
        if (isset($mybb->input['uhide']) && is_member($mybb->settings[CN_ABPUMAP . '_gallowhide'])) {
            $datas['hide'] = 1;
        }
        $db->update_query(CN_ABPUMAP . 'users', $datas, "uid=" . (int) $mybb->user['uid']);
        abp_umap_cache();
        redirect('usercp.php?action=abp_umap', $message);
    } elseif ($mybb->input['action'] == 'abp_umap') {
        add_breadcrumb($lang->nav_usercp, "usercp.php");
        add_breadcrumb($lang->abp_umapname, "usercp.php?action=abp_umap");
        $usettings = umap_get_settings();
        list($iw, $ih) = abp_marker_info($usettings['marker']);
        $umap_zoom = $usettings['zoom'];
        $umap_marker = $usettings['marker'];
        switch ($usettings['hmarker']) {
            case 'left': $hof = $iw;
                break;
            case 'right': $hof = 0;
                break;
            default: $hof = $iw / 2;
                break;
        }
        switch ($usettings['vmarker']) {
            case 'top': $vof = $ih;
                break;
            case 'bottom': $vof = 0;
                break;
            default: $vof = $ih / 2;
                break;
        }
        $scalecontrol = '';
        if ($usettings['scontrol'] == 'yes') {
            $scalecontrol = 'L.control.scale().addTo(abp_umap);';
        }
        //if (isset($mybb->user['aum_latlng']) && (string) $mybb->user['aum_latlng'] != '') {
        if ($mybb->user['islocalised'] === true) {
            $users = $cache->read(CN_ABPUMAP);
            $lat = $users[$mybb->user['uid']]['lat'];
            $lon = $users[$mybb->user['uid']]['lon'];
            //list($lat, $lng) = explode(',', $mybb->user['aum_latlng']);
            $abp_umap_location_not_set = '';
        } else {
            list($lat, $lon) = explode(',', $usettings['latlng']);
            $abp_umap_location_not_set = $lang->abp_umap_location_not_set;
        }
        $umap_center = "lat:" . (float) $lat . ", lon:" . (float) $lon;
        $ulatlng = (float) $lat . ',' . (float) $lon;
        if ((int) $mybb->settings[CN_ABPUMAP . '_autolocate'] == 1) {
            $lang->abp_umap_deleteloc = $lang->abp_umap_resetloc;
            $disabled = ' disabled="disabled"';
            if (is_member($mybb->settings[CN_ABPUMAP . '_gallowhide'])) {
                if ($users[$mybb->user['uid']]['hide'] == 1) {
                    $hidechecked = ' checked="checked"';
                } else {
                    $hidechecked = '';
                }
                eval("\$umaphide = \"" . $templates->get(UMAP_TPL . '_hide') . "\";");
            }
        } else {
            $disabled = '';
            $umaphide = '';
        }
        eval("\$page = \"" . $templates->get(UMAP_TPL . '_ucp') . "\";");
        output_page($page);
    }
}

/** Usermap page * */
$plugins->add_hook('misc_start', 'abp_umap');

/**
 * Creates the usermap page
 * @global MyLanguage $lang
 */
function abp_umap() {
    global $mybb, $templates, $theme, $lang, $header, $headerinclude, $footer, $db, $cache, $abp_umap_users, $abp_umap_hidden_cpt;
    if ($mybb->input['action'] != CN_ABPUMAP) {
        return;
    }
    if (!is_member($mybb->settings[CN_ABPUMAP . '_gallowed'])) {
        error_no_permission();
    }

    $lang->load(CN_ABPUMAP);
    $usettings = umap_get_settings();

    $ulat = $ulon = 0;
    $abp_umap_recenter = $lang->abp_umap_centerme;
    $cptusers = 0;
    list($lat, $lon) = explode(',', $usettings['latlng']);
    abp_umap_cache();
    $users = $cache->read(CN_ABPUMAP);

    // Check if user hidded and if in allowed groups
    // @TODO
    if ($mybb->settings[CN_ABPUMAP . '_blockhidders'] == 1 && $users[$mybb->user['uid']]['hide'] == 1) {
        error_no_permission();
    }
    foreach ($users as $user) {
        if ($user['hide'] == 1) {
            continue;
        }
        $cptusers++;
        $udata[$user['uid']] = [
            'lat' => $user['lat'],
            'lon' => $user['lon'],
            'img' => abp_make_popup($user, $usettings['defavatar'])
        ];
        if ($user['uid'] == $mybb->user['uid']) {
            $ulat = (float) $user['lat'];
            $ulon = (float) $user['lon'];
        }
    }
    $abp_umap_userlist = '';
    if (isset($mybb->settings['abp_umap_userlist']) && $mybb->settings['abp_umap_userlist'] == 1) {
        $abp_umap_userlist = $lang->sprintf($lang->abp_umap_userlist, $abp_umap_users);
    }
    if ($ulat == 0 && $ulon == 0) {
        $ulat = (float) $lat;
        $ulon = (float) $lon;
        $abp_umap_recenter = $lang->abp_umap_recenter;
    }
    $umap_zoom = $usettings['zoom'];
    list($iw, $ih) = abp_marker_info($usettings['marker']);

    $umap_center = "lat:" . (float) $lat . ", lon:" . (float) $lon;
    $umap_users = 'var umusers = ' . json_encode($udata);
    $umap_marker = $usettings['marker'];
    $umap_class = $usettings['uclass'];
    switch ($usettings['hmarker']) {
        case 'left': $hof = $iw;
            break;
        case 'right': $hof = 0;
            break;
        default: $hof = $iw / 2;
            break;
    }
    switch ($usettings['vmarker']) {
        case 'top': $vof = $ih;
            break;
        case 'bottom': $vof = 0;
            break;
        default: $vof = $ih / 2;
            break;
    }
    $scalecontrol = '';
    if (isset($usettings['scontrol']) && $usettings['scontrol'] == 'yes') {
        $scalecontrol = 'L.control.scale().addTo(abp_umap);';
    }
    $disabcluster = '';
    $userzoom = 14;
    if (isset($usettings['disabcluster']) && (int) $usettings['disabcluster'] >= 0 && (int) $usettings['disabcluster'] < 19) {
        $disabcluster = '{disableClusteringAtZoom:' . (int) $usettings['disabcluster'] . ', spiderfyOnMaxZoom: true}';
        $userzoom = (int) $usettings['disabcluster'];
    }
    $abp_umap_stats = $lang->sprintf($lang->abp_umap_stats, $cptusers);

    if ($mybb->get_input('action') == 'abp_umap') {
        add_breadcrumb($lang->abp_umapname, "misc.php?action=abp_umap");
        eval("\$page = \"" . $templates->get(UMAP_TPL . '_page') . "\";");
        output_page($page);
    }
}

/**
 * Returns info about marker, or false if failed
 * @param string $marker path to the marker
 * @param boolean $check Force to return only 'is valid picture'
 * @return array | boolean
 */
function abp_marker_info($marker, $check = false) {
    $file = str_replace(
            ['/', '\\'], [DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR], MYBB_ROOT . $marker
    );
    $iw = $ih = 0;
    $ispic = false;
    if (is_file($file)) {
        $arrimg = getimagesize($file);
    }
    if (is_array($arrimg)) {
        list($iw, $ih, $type, $attr) = $arrimg;
        $ispic = true;
    }
    if ($check === true) {
        return $ispic;
    } else {
        return [$iw, $ih, $ispic];
    }
}

/**
 * Format the popup, based on settings
 * @global MyBB $mybb
 * @param array $user User informations
 * @param int $defavatar Force usage of an avatar 
 * @return string
 */
function abp_make_popup($user, $defavatar = 0) {
    global $mybb, $templates;
    if (trim($user['avatar']) == '' && $defavatar == 1) {
        $user['avatar'] = './images/default_avatar.png';
    }
    if ($mybb->settings['abp_umap_adisplay'] == 1 && trim($user['avatar']) != '') {
        $avatar = $user['avatar'];
        eval("\$abpumap_popup_avatar = \"" . $templates->get(UMAP_TPL . '_popup_avatar') . "\";");
    }
    $username = abp_umap_makeuser($user);
    eval("\$abpumap_popup = \"" . $templates->get(UMAP_TPL . '_popup') . "\";");

    return $abpumap_popup;
}

$plugins->add_hook('global_intermediate', 'abp_umap_toplink');

/**
 * Add the top link
 * @global MyBB $mybb
 * @global type $templates
 * @global MyLanguage $lang
 * @global datacache $cache
 * @global type $abp_umap_toplink
 * @global int $abp_umap_cpt
 * @global string $abp_umap_users
 */
function abp_umap_toplink() {
    global $mybb, $templates, $templatelist, $lang, $abp_umap_toplink, $abp_umap_warnuser, $current_page;
    $warnpage = ['index.php', 'showthread.php', 'forumdisplay.php', 'newthread.php', 'newreply.php', 'polls.php', 'search.php'];
    $lang->load(CN_ABPUMAP);
    if ((int) $mybb->user['uid'] > 0 && in_array($current_page, $warnpage) && $mybb->settings[CN_ABPUMAP . '_warnuser'] == 1 && !$mybb->user['islocalised']) {
        $abp_umap_warningnotset = $lang->sprintf($lang->abp_umap_warningnotset, $mybb->settings['bburl']);
        eval("\$" . CN_ABPUMAP . "_warnuser =\"" . $templates->get(UMAP_TPL . '_warnuser') . "\";");
    } else {
        eval("\$" . CN_ABPUMAP . "_warnuser =\"\";");
    }
    eval("\$" . CN_ABPUMAP . "_toplink = \"" . $templates->get(UMAP_TPL . '_toplink') . "\";");
}

$plugins->add_hook('global_start', 'abp_umap_global');

/**
 * Add some globals variables, accessibles from everywhere
 * @global MyBB $mybb
 * @global datacache $cache
 * @global int $abp_umap_cpt Number of localised users
 * @global int $abp_umap_hidden_cpt Number of localised but hidden users
 * @global string $abp_umap_users List of localised users
 */
function abp_umap_global() {
    global $mybb, $cache, $abp_umap_cpt, $abp_umap_hidden_cpt, $abp_umap_users, $templatelist;
    if ((string) $templatelist != '') {
        $templatelist .= ', ';
    }
    // Load templates in cache
    $templatelist .= UMAP_TPL . '_toplink, ' . UMAP_TPL . '_warnuser';
    if (THIS_SCRIPT == 'misc.php' && $mybb->input['action'] == CN_ABPUMAP) {
        $templatelist .= ', ' . UMAP_TPL . '_page, ' . UMAP_TPL . '_popup, ' . UMAP_TPL . '_popup_avatar';
    } elseif (THIS_SCRIPT == 'usercp.php') {
        $templatelist .= ', ' . UMAP_TPL . '_nav_option';
        if ($mybb->input['action'] == CN_ABPUMAP) {
            $templatelist .= ', ' . UMAP_TPL . '_ucp, ' . UMAP_TPL . '_hide';
        }
    }
    $mybb->user['islocalised'] = false;
    $umap_users = $cache->read(CN_ABPUMAP);
    $arr_userlist = $arr_hiddenuser = [];
    $abp_umap_users = '';
    $abp_umap_cpt = count($umap_users);
    $abp_umap_hidden_cpt = 0;
    if (!array_key_exists($mybb->user['uid'], $umap_users) && $mybb->settings[CN_ABPUMAP . '_autolocate'] == 1) {
        abp_umap_autolocate($mybb->user['uid']);
        $umap_users = $cache->read(CN_ABPUMAP);
    }
    foreach ($umap_users as $umap_user) {
        if ($mybb->user['uid'] == $umap_user['uid']) {
            $mybb->user['islocalised'] = true;
        }
        if ($umap_user['hide'] == 1) {
            $abp_umap_hidden_cpt++;
            $arr_hiddenuser[] = abp_umap_makeuser($umap_user);
        } else {
            $arr_userlist[] = abp_umap_makeuser($umap_user);
        }
    }
    if (count($arr_userlist) > 0) {
        $abp_umap_users = implode(', ', $arr_userlist);
    }
}

/**
 * Generate the user link
 */
function abp_umap_makeuser($uuser) {
    global $mybb;
    if ($mybb->settings['abp_umap_gcolor'] == 1) {
        $madeuser = format_name(htmlspecialchars_uni($uuser['username']), $uuser['usergroup'], $uuser['displaygroup']);
    } else {
        $madeuser = $uuser['username'];
    }
    if ($mybb->settings['abp_umap_ulink'] == 1) {
        $madeuser = '<a id="' . $uuser['uid'] . '" class="popmap" href="' . get_profile_link($uuser['uid']) . '" title="' . $uuser['username'] . '">' . $madeuser . '</a>';
    }
    return $madeuser;
}

/** WOL * */
$plugins->add_hook('fetch_wol_activity_end', 'abp_umap_wol');
$plugins->add_hook('build_friendly_wol_location_end', 'abp_umap_build_wol');

/**
 * 
 * @param array $user_activity
 */
function abp_umap_wol(&$user_activity) {
    if ($user_activity['activity'] != 'misc') {
        return;
    }
    if (my_strpos($user_activity['location'], 'abp_umap') === false) {
        return;
    }
    $user_activity['activity'] = 'Usermap';
}

/**
 * 
 * @global MyLanguage $lang
 * @param array $user_activity
 */
function abp_umap_build_wol(&$user_activity) {
    global $lang;
    if ($user_activity['user_activity']['activity'] == 'Usermap') {
        $lang->load(CN_ABPUMAP);
        $user_activity['location_name'] = $lang->sprintf($lang->abp_umap_wol, 'misc.php?action=abp_umap');
    }
}

$plugins->add_hook('postbit', 'abp_umap_ulocated');

/**
 * Instanciate a global variable to use in other plugins
 * @global boolean $abp_umap_user
 * @global cache $cache
 * @param array $post
 */
function abp_umap_ulocated(&$post) {
    global $abp_umap_user, $cache;
    $umap_users = $cache->read(CN_ABPUMAP);
    $abp_umap_user = array_key_exists($post['uid'], $umap_users);
}

/*
 * Retrieves settings for umap
 * @global 
 */

function umap_get_settings() {
    global $db;
    $settings = [];
    $query = $db->simple_select(CN_ABPUMAP);
    while ($setting = $db->fetch_array($query)) {
        $settings[$setting['sname']] = $setting['svalue'];
    }
    return $settings;
}

$plugins->add_hook('usercp_do_avatar_end', 'abp_umap_cache');

/**
 * Actually not used
 * @global DB_MySQLi $db
 * @global type $cache
 */
function abp_umap_cache() {
    global $db, $cache, $mybb;
    $query = $db->write_query("SELECT u.uid, u.username, u.usergroup, u.additionalgroups, u.displaygroup, u.avatar, g.lat, g.lon, g.hide FROM " . TABLE_PREFIX . CN_ABPUMAP . "users g JOIN " . TABLE_PREFIX . "users u ON u.uid=g.uid WHERE g.lat<>0 AND g.lon<>0 ORDER BY " . $mybb->settings[CN_ABPUMAP . '_usersort'] . " " . $mybb->settings[CN_ABPUMAP . '_usorder']);
    $usermap = array();
    while ($user = $db->fetch_array($query)) {
        if (!is_membergroup($mybb->settings[CN_ABPUMAP . '_gallowed'], $user['usergroup'], $user['additionalgroups'])) {
            continue;
        }
        $usermap[$user['uid']] = $user;
        $usermap[$user['uid']]['lat'] = (float) $user['lat'];
        $usermap[$user['uid']]['lon'] = (float) $user['lon'];
        $usermap[$user['uid']]['hide'] = (int) $user['hide'];
    }
    $cache->update(CN_ABPUMAP, $usermap);
}

/*
 * Alternative to is_member
 * Don't use SQL query
 */

function is_membergroup($groups, $usergroup = 0, $additionalgroups = '') {
    $memberships = array_map('intval', explode(',', $additionalgroups));
    $memberships[] = $usergroup;
    if (!is_array($groups)) {
        if ((int) $groups == -1) {
            return $memberships;
        } else {
            if (is_string($groups)) {
                $groups = explode(',', $groups);
            } else {
                $groups = (array) $groups;
            }
        }
    }
    $groups = array_filter(array_map('intval', $groups));
    return array_intersect($groups, $memberships);
}

function abp_umap_autolocate($uid, $force = 0, $ip = '') {
    global $mybb, $cache, $db;
    if ($mybb->settings[CN_ABPUMAP . '_autolocate'] == 0) {
        return;
    }
    if ((int) $uid < 1) {
        return;
    }
    $umap_users = $cache->read(CN_ABPUMAP);
    if (!array_key_exists($uid, $umap_users) || $force === 1) {
        try {
            if ($ip == '') {
                $ip = get_ip();
            }
            $datas = file_get_contents('http://ip-api.com/json/' . $ip);
            if ($datas) {
                $json = json_decode($datas);
                if ($json->status != 'success') {
                    $db->replace_query(CN_ABPUMAP . 'users', ['uid' => $uid, 'lat' => 0.0, 'lon' => 0.0]);
                    return '0.00,0.00';
                } else {
                    $db->replace_query(CN_ABPUMAP . 'users', [
                        'uid' => $uid,
                        'lat' => $json->lat,
                        'lon' => $json->lon,
                        'hide' => 0,
                        'country' => $db->escape_string($json->country),
                        'countrycode' => $db->escape_string(strtolower($json->countryCode)),
                        'region' => $db->escape_string($json->regionName),
                        'city' => $db->escape_string($json->city),
                        'zip' => $json->zip,
                        'autoloc' => 1
                    ]);
                }
                abp_umap_cache();
                return $json->lat . ',' . $json->lon;
            }
        } catch (Exception $e) {
            
        }
    }
}

function abp_umap_geo2loc($uid, $lat = 0.00, $lon = 0.00, $fakeb = false) {
    global $mybb, $db;
    if ($fakeb === true) {
        $browser = 'Lynx/2.8.9dev.11 libwww-FM/2.14 SSL-MM/1.4.1 GNUTLS/3.5.6';
    } else {
        $browser = $_SERVER['HTTP_USER_AGENT'];
    }
    ini_set('user_agent', $browser);
    try {
        $datas = file_get_contents('https://nominatim.openstreetmap.org/reverse?format=json&lat=' . $lat . '&lon=' . $lon);
        if ($datas) {
            $json = json_decode($datas);
            $geoloc = [
                'uid' => $uid,
                'lat' => $lat,
                'lon' => $lon,
                'country' => $db->escape_string($json->address->country),
                'countrycode' => $db->escape_string(strtolower($json->address->country_code)),
                'region' => $db->escape_string($json->address->state),
                'city' => $db->escape_string($json->address->village),
                'zip' => $json->address->postcode,
                'autoloc' => 1
            ];
            $db->replace_query(CN_ABPUMAP . 'users', $geoloc);
        } else {
            $db->replace_query(CN_ABPUMAP . 'users', ['uid' => $uid, 'zip' => '00000']);
        }
    } catch (Exception $e) {
        
    }
}

function abp_umap_fillufield($uid) {
    global $db;
    $usettings = umap_get_settings();
    if ((int) $usettings['pfield'] == 0 || trim($usettings['pfield_format']) == '') {
        return;
    }
    if (!$db->field_exists('fid' . $usettings['pfield'], 'userfields')) {
        return;
    }
    $query = $db->simple_select(CN_ABPUMAP . 'users', '*', "uid=" . $uid);
    $res = $db->fetch_array($query);
    if ((int) $res['zip'] != 0) {
        $fvalue = str_replace(
                ['%country%', '%ccode%', '%region%', '%city%', '%zip%'],
                [$res['country'], $res['countrycode'], $res['region'], $res['city'], $res['zip']],
                $usettings['pfield_format']
        );
        $query = $db->update_query('userfields', ['fid' . $usettings['pfield'] => $db->escape_string($fvalue)], "ufid=" . $uid);
    }
    return;
}

function debug($line) {
    $fo = fopen(MYBB_ROOT . '/uploads/mydebug.log', 'a');
    fputs($fo, print_r($line, true) . PHP_EOL);
    fclose($fo);
}
